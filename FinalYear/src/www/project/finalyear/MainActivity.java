package www.project.finalyear;


import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {  
	// #GLobal variables
	public final static String BASE_URL = "http://";
	public final static String EXTRA_MESSAGE = "com.www.finalyear";
	public HttpURLConnection urlCon = null;
	public HttpClient httpclient = new DefaultHttpClient();;
	public ProgressBar progressBar;
	public String responseBody; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressBar.setVisibility(View.GONE);
		Button button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onClick(View view) {
		//response of button
		EditText editText = (EditText) findViewById(R.id.editText1);
		String message = editText.getText().toString();
		
		if(message.length() < 1) {
			Toast.makeText(this, "Please enter URL", Toast.LENGTH_LONG).show();
		} else {
			 progressBar.setVisibility(View.VISIBLE);
			 new MyAsyncHttpClient().execute(message);
		}
    }
	
	public void createIntent() {
		//create a keyword activity
		Intent intent = new Intent(this, KeywordActivity.class);		
		intent.putExtra(EXTRA_MESSAGE, responseBody);
		startActivity(intent);
    	
			
	}  	
	
     private class MyAsyncHttpClient extends AsyncTask<String, Integer, Double> {
    	// #Global variables to class MyAysncHttpClient
    	Double res = null;
    	String message = null;

    	@Override
		protected Double doInBackground(final String... params) {
			// TODO Auto-generated method stub
    		message = params[0];
    		System.out.println("value of PARAMS " + message);
    		//Establish HTTP Connection
       			try {
					URL url;;
					String address  =  BASE_URL + params[0];
					url = new URL(address);
					urlCon = (HttpURLConnection) url.openConnection();
					urlCon.setDoInput(true);
					urlCon.setDoOutput(true);
					urlCon.setUseCaches(false);
					urlCon.setRequestMethod("POST");
					urlCon.setChunkedStreamingMode(100);
					urlCon.setRequestProperty("Content-Type", "json/application");//
					urlCon.connect();
					System.out.println("COnnected !!");
					
					//Send Post Output 
					byte [] outputBytes = "{'value': 7.5}".getBytes("UTF-8");
					OutputStream os = urlCon.getOutputStream();
					os.write(outputBytes);
					//Whats the Value #PRINT
					System.out.println("outputstream" + os);
					os.close();	
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println(e);
					res = 1.0;
				} finally {
					if(urlCon != null) {
						urlCon.disconnect();
					}
				}   			
	    	  	
	    		JSONObject json = new JSONObject();
	    		try {
	    			json.put("URL", message);
	    			HttpGet httpPost = new HttpGet("http://10.0.2.2:3000/url?url=" + json.get("URL"));
	    			System.out.println("Where is the response??");
	    			
	    			//execute http response
	     			HttpResponse response = null;
	     			response = httpclient.execute(httpPost);
	     			
	     			//Whats the Value #PRINT
	    			System.out.println("here is the response '\n' The result of the Response: " + response);
				
	    			if(response != null) {
	    				//InputStream in = response.getEntity().getContent();  
	    				responseBody = EntityUtils.toString(response.getEntity());
	    				System.out.println("here is the responseBody '\n' The result of the Response: " + responseBody);
		     			
	    			}
				
	    		}catch (Exception e1) {
	    			// TODO Auto-generated catch block
	    			e1.printStackTrace();
	    			System.out.println("You have an error to establish connection");
	    		}
	    		//Whats the Value #PRINT
	    		System.out.println("Json object on Main: " + json);
	    		   
	    		//creating intent of keyword activity
	    	
     	   return null;
		} 
    	
    	@Override
    	protected void onPostExecute(Double result) {
		 
	    	if(res == null) {
	    		System.out.println("Success !! onPostExceute");
	    		progressBar.setVisibility(View.GONE);
	    		Toast.makeText(getApplicationContext(), "success!!" , Toast.LENGTH_LONG).show();
	    		createIntent();  		
	    		System.out.println("New Avtivity created");	
	    	}else {
	    		progressBar.setVisibility(View.GONE);
	    		Toast.makeText(getApplicationContext(), "Sorry Try again !!", Toast.LENGTH_LONG).show();
	    	}
	    	
	    }
	  	
	    protected void onProgressUpdate(Integer... progress) {
	    	progressBar.setProgress(progress[0]);
	    	//Toast.makeText(getApplicationContext(),"working", Toast.LENGTH_LONG).show();
	    }
	    
     }
}