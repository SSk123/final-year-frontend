package www.project.finalyear;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class KeywordActivity extends Activity implements OnItemSelectedListener{
	public static String[] items = new String[]{"Keywords"};
	public JSONArray jArray = new JSONArray();
	public JSONObject json = new JSONObject();
	public String responseBody = null;
	public HttpClient httpclient = new DefaultHttpClient();   
    		
	@SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyword);

        // Make sure we're running on Honeycomb or higher to use ActionBar APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
       // Get the message from the intent
       Intent intent = getIntent();
       String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
       System.out.println("Response in Keyword activity: " + message);
       
       //TextView
       TextView textView = (TextView)findViewById(R.id.textView1);
       textView.setTextSize(30);
       textView.setText(message);

       
       spinner_func();
   	   // Spinner spinner = (Spinner) findViewById(R.id.spinner1);
   	   // spinner.setOnItemSelectedListener(this);
   	}

	public void onItemSelected(AdapterView<?> parent, View view, 
            int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        Object key = parent.getItemAtPosition(pos);
        System.out.println("The selected item from spinner: " + key);
     
        EditText editText = (EditText) findViewById(R.id.editText1);
        editText.setText((String)key);
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
    
    public void spinner_func() {	
    	Spinner dropdown = (Spinner)findViewById(R.id.spinner1);
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        dropdown.setAdapter(adapter);   
    }
    
    private class MyAsyncHttpClient extends AsyncTask<String, Integer, Double> {
    	// #Global variables to class MyAysncHttpClient
    	Double res = null;
    	String message = null;

   		@Override
		protected Double doInBackground(final String... params) {
			// TODO Auto-generated method stub
   			message = params[0];
   			System.out.println("value of PARAMS " + message);
    		try {
    			json.put("Keyword", message);
	    		HttpGet httpPost = new HttpGet("http://10.0.2.2:3000/keywords?url=" + json.get("Keyword"));
	    		System.out.println("Where is the response??");
	    		
	    		//execute http response
	     		HttpResponse response = null;
	     		response = httpclient.execute(httpPost);
	     		
	     		//Whats the Value #PRINT
	    		System.out.println("here is the response '\n' The result of the Response: " + response);
				
	    		if(response != null) {
	    			//InputStream in = response.getEntity().getContent();  
	    			responseBody = EntityUtils.toString(response.getEntity());
	    			System.out.println("here is the responseBody for Keyword '\n' The result of the Response: " + responseBody);
	    		       //TextView
	    				TextView textView = (TextView)findViewById(R.id.textView1);
	    				textView.setTextSize(30);
	    				textView.setText(responseBody);		
	    		}
				
	    	}catch (Exception e1) {
	    		// TODO Auto-generated catch block
	    		e1.printStackTrace();
	    		System.out.println("You have an error to establish connection");
	    	}
	    	//Whats the Value #PRINT
	    	System.out.println("Json object on Main: " + json);
	    	
    	  return null;
   		} 
   	
   		@Override
   		protected void onPostExecute(Double result) {
	    	if(res == null) {
	    		System.out.println("Success !! onPostExceute");
	    		Toast.makeText(getApplicationContext(), "success!!" , Toast.LENGTH_LONG).show();	
	    	}else {
	    		Toast.makeText(getApplicationContext(), "Sorry Try again !!", Toast.LENGTH_LONG).show();
	    	}
	    }
	  	
	    protected void onProgressUpdate(Integer... progress) {
	    	Toast.makeText(getApplicationContext(),"working", Toast.LENGTH_LONG).show();
	    }
	    
    }     
    public void sendKeyword(View view){
    	//keyword is added
    	EditText editText = (EditText) findViewById(R.id.editText1);
    	String keyword = editText.getText().toString();
    	if(keyword.length() >= 1 ) {
    	items = push(items, keyword);
       	spinner_func();
       	new MyAsyncHttpClient().execute(keyword);
    	}
    	JSONObject json = new JSONObject();
    	try {
    		json.put("spinner object", keyword);
    		jArray.put(json);
    		System.out.println("Json object on Keyword: " + json);
       	} 
    	catch(JSONException e) {
    		// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.toString());
    	}	
    }
    
    private static String[] push(String[] array, String data) {
        String[] longer = new String[array.length + 1];
        System.arraycopy(array, 0, longer, 0, array.length);
        longer[array.length] = data;
        return longer;
    }
    
    public void resetField(View view) {
    	//reset the editText field
    	EditText editText=(EditText) findViewById(R.id.editText1);
    	editText.setText("");
    }
}


